class HomeController < ApplicationController
  def index
    page_no          = params[:page_no] || 0
    records          = JSON.parse(File.read(Rails.root.to_s + '/universities_rankings.json'))
    @universities    = records.slice(page_no.to_i * 10, 10)
    @number_of_pages = records.length/10
  end
end
