desc "Fetch universities raking"
task :scrape_rankings => :environment do
  require 'open-uri'
  result = []
  page_no = 1
  begin
    while true
      url = "http://colleges.usnews.rankingsandreviews.com/best-colleges/rankings/national-universities/data?_page=#{page_no}"
      file = open(url)
      doc = Nokogiri::HTML(file)
      doc.css("#search-application-results-view tr").each_with_index do |university, index|
        next if index == 0
        row = university.css("td")
        next if row.length != 4
        information = {}
        information['name'] = row[0].css(".text-strong.text-large").text.strip
        information['rank'] = row[0].css('.text-small span').text[/[0-9]+/]
        information['tution_fee'] = row[1].css("div").text.strip
        information['enrollemnts'] = row[2].css("div").text.strip
        result << information
      end
      page_no += 1
    end
  rescue OpenURI::HTTPError => e
  end
  #write the result into a json file
  File.open('universities_rankings.json', 'w') { |file| file.write(result.to_json) }
end

