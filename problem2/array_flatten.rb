module ArrayFlatten
  def self.make_flat array
    result = []
    array.each do |element|
      if element.kind_of?(Fixnum)
        result << element
      elsif element.kind_of?(Array)
        flattened_array = self.make_flat(element)
        flattened_array.each do |el|
          result << el
        end
      else
        raise "InvalidFormat", "Array element should be a Fixnumber"
      end
    end
    result.sort
  end
end
