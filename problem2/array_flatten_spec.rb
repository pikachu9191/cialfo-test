require_relative 'array_flatten'
describe 'ArrayFlatten' do
  it "flattens and sorts a given array" do
    ary = [1, 2, 3, [4, 6, [7, 8]], 5, 9, 10]
    expect ArrayFlatten.make_flat(ary) == ary.flatten.sort
  end
end
