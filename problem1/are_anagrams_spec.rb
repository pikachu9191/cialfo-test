require_relative 'are_anagrams'
describe 'AreAnagrams' do
  it "true when 2 strings are anagrams" do
    str1   = "abCd"
    str2   = "Dcba"
    result = AreAnagrams.are_anagrams? str1, str2
    
    expect result == true
  end
  it "false when two strings are not anagrams" do
    str1   = "abCd"
    str2   = "Dcbae"
    result = AreAnagrams.are_anagrams? str1, str2
    
    expect result == false
  end
end
