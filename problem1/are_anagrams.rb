module AreAnagrams
  def self.are_anagrams?(string_a, string_b)
    sort_string(string_a) == sort_string(string_b)
  end
  private
  def self.sort_string string
    string.downcase.chars.sort.join
  end
end
